'use strict';

const mongoose = require("mongoose");
const connectionConfig = require('./db_config_factory');

mongoose.connect(connectionConfig.connection);

const db = mongoose.connection;
db.on("error", (err) => console.error(err));

// Use JavaScript ES2016 promises
mongoose.Promise = Promise;

module.exports = mongoose;