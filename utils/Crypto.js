'use strict';

const crypto_config = require('../config/crypto.json')
    , crypto = require('crypto');

class Crypto {
    static sha256hash(password) {
        return crypto.createHmac('sha256', crypto_config.secret)
            .update(password)
            .digest('hex');
    }
}

module.exports = Crypto;