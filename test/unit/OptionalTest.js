'use strict';

const assert = require('chai').assert
    , Optional = require('../../modules/api/java/Optional');

describe('Optional', () => {

    describe('#orElse', () => {
        it('value exist', () => {
            const TEST_VALUE = 'vjacheslav';

            let testingValue = Optional.valueOf(TEST_VALUE)
                .orElse('else value');

            assert.equal(TEST_VALUE, testingValue);
        });

        it('value not exist', () => {
            const OR_ELSE_VAL = 'thisIsOrElse';

            let orElseVal = Optional.valueOf()
                .orElse(OR_ELSE_VAL);

            assert.equal(OR_ELSE_VAL, orElseVal);
        });
    });

    describe('#orElseGet', () => {
        it('value exist', () => {
            const TEST_VALUE = 'java best language';

            let value = Optional.valueOf(TEST_VALUE)
                .orElseGet(() => "changed");

            assert.equal(TEST_VALUE, value);
        });

        it('value not exist', () => {
            const EXPECTED = "java cool";

            let actual = Optional.valueOf()
                .orElseGet(() => EXPECTED);

            assert.equal(EXPECTED, actual);
        });
    });

    describe("#isPresent", () => {
        it("false", () => {
            let optional = Optional.valueOf();
            assert.isFalse(optional.isPresent());
        });

        it('true', () => {
            const VALUE = "java";
            let optional = Optional.valueOf(VALUE);
            assert.isTrue(optional.isPresent());
        });
    });

    describe("#ifPresent", () => {
        it("value presented", () => {
            const PRESENT_VALUE = "java";

            Optional.valueOf(PRESENT_VALUE).ifPresent((val) => {
                assert.equal(PRESENT_VALUE, val);
            });
        });

        it("value not presented", () => {
            let isCallable = false;

            Optional.valueOf().ifPresent(() => {
                isCallable = true;
            });

            assert.isFalse(isCallable);
        });
    });

    describe("#get", () => {
        it("value present", () => {
            const EXPECTED = "java";
            let value = Optional.valueOf(EXPECTED).get();
            assert.equal(EXPECTED, value);
        });

        it("value not present", () => {
            let nullable = Optional.valueOf().get();
            assert.isUndefined(nullable);
        });
    });

    describe('#orElseLambda', () => {
        it("value not present", (done) => {
            Optional.valueOf().orElseLambda(() => done());
        });

        it("value present", () => {
            let isCalled = false;

            Optional.valueOf("test").orElseLambda(() => (isCalled = true));

            assert.isFalse(isCalled);
        });
    });
});