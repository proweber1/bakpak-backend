'use strict';

const assert = require('chai').assert
    , UrlBuilder = require('../../modules/web/utils/UrlBuilder');

describe('UrlBuilder', () => {
    it('-> exist token', () => {
        assert.equal('/web/confirm/XWuiYdlj', UrlBuilder.getConfirmationUrl('XWuiYdlj'));
    });

    it('-> token not exist', () => {
        assert.isFalse(UrlBuilder.getConfirmationUrl());
    });
});