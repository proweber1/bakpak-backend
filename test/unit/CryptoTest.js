'use strict';

const assert = require('chai').assert
    , Crypto = require('../../utils/Crypto')
    , nodeCrypto = require('crypto')
    , crypto_config = require('../../config/crypto.json');

describe('Crypto', () => {
    it("#sha256", () => {
        const PASSWORD = "XWuiYdlj";
        const EXCEPT = nodeCrypto.createHmac('sha256', crypto_config.secret)
            .update(PASSWORD)
            .digest('hex');

        assert.equal(EXCEPT, Crypto.sha256hash(PASSWORD));
    });
});