'use strict';

const assert = require('chai').assert
    , User = require('../../models/User')
    , UserManager = require('../../managers/UserManager')
    , Crypto = require('../../utils/Crypto')
    , Optional = require('../../modules/api/java/Optional');

/**
 * Тестирование пользователя
 *
 * @param user
 * @param email
 * @param password
 */
function testUser(user, email, password) {
    assert.isNotNull(user);
    assert.isDefined(user.email);
    assert.isDefined(user.password);
    assert.isDefined(user.email_confirmed);
    assert.isDefined(user.email_confirm_token);

    assert.equal(email, user.email);
    assert.isFalse(user.email_confirmed);
    assert.isString(user.email_confirm_token);

    const password_hash = Crypto.sha256hash(password);

    assert.equal(password_hash, user.password);
}

/**
 * Тестирование UserManager'а
 */
describe('User manager', () => {
    let test_email = "bakpak@elcamino.edu";
    let test_pass = "XWuiYdlj";

    /**
     * Создаем пользователя с корректными данными
     */
    it("#create is valid data", function(done) {
        UserManager.createUser(test_email, test_pass)
            .then((createdUser) => {
                testUser(createdUser, test_email, test_pass);

                return User.findOne({ email: test_email });
            })
            .then((findUser) => {
                testUser(findUser, test_email, test_pass);

                return User.find({ email: test_email }).remove().exec();
            })
            .then(() => {
                done();
            });
    });

    // TODO: Написать тест ситуации вставки невалидных данных

    /**
     * Тестируем поиск пользователя через UserManager
     */
    it("#finding by email", (done) => {
        let savedUser;

        /*
        Создает нового пользователя. Эта схема
        может меняться, по-этому не забываем менять
        тест.
         */
        let promise = new User({
            email: test_email,
            password: Crypto.sha256hash(test_pass),
            email_confirmed: false,
            email_confirm_token: "confirm_token"
        }).save();

        promise
            .then((user) => {
                savedUser = user;

                return UserManager.findUser(test_email);
            })
            .then((managerUser) => {
                assert.isTrue(managerUser instanceof Optional);

                const user = managerUser.get();

                assert.isNotNull(managerUser.get());
                assert.equal(user.email, savedUser.email);
                assert.equal(user.password, savedUser.password);

                return User.find({ email: test_email }).remove();
            })
            .then(() => done());
    });
});