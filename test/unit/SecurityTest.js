'use strict';

const Security = require("../../modules/api/services/Security")
    , assert = require("chai").assert
    , Crypto = require("../../utils/Crypto");

describe('Security', () => {
    describe("#validatePassword", () => {
        const TEST_PASSWORD = "XWuiYdlj";
        const password_hash = Crypto.sha256hash(TEST_PASSWORD);

        it("is validate password true", () => {
            assert.isTrue(Security.validatePassword(TEST_PASSWORD, password_hash));
        });

        it("is validate password false", () => {
            assert.isFalse(Security.validatePassword("bikpak", password_hash));
        });
    });
});