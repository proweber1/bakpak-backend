'use strict';

const chai = require('chai')
    , assert = chai.assert
    , app = require('../../app')
    , chaiHttp = require('chai-http')
    , User = require('../../models/User');

chai.use(chaiHttp);

const agent = chai.request.agent(app)
    , REGISTRATION_URL = "/api/v1/registration";

/**
 * Тестирование контроллера регистрации
 */
describe('Registration', () => {

    /**
     * Проверяем что можно регать только @elcamino email'ы
     */
    describe('-> invalid email, elcamino', () => {

        /**
         * Посылаем запрос с неправильным email'ом, который не
         * соответствует паттерну @elcamino.edu. Должна по идее
         * быть ошибка валидации.
         */
        it('#isInvalidElcaminoEmail', (done) => {
            agent.post(REGISTRATION_URL)
                .send({ email: 'wwwwwwwwww@mail.ru', password: 'XWWWWWWWW' })
                .end((err, res) => {
                    assert.equal(400, res.statusCode);
                    let response = JSON.parse(res.text);

                    assert.isDefined(response.errors);
                    assert.equal(1, response.errors.length);
                    assert.isArray(response.errors);
                    assert.equal('Valid only @elcamino.edu the emails', response.errors[0].message);

                    done();
                });
        });
    });

    /**
     * Проверяем ситуацию когда мы пытаемся зарегистрировать
     * пользователя с e-mail'ом который уже есть в системе
     */
    describe('-> userExists', () => {

        const TEST_EMAIL = "axWuiyd@elcamino.edu";

        /**
         * Это выполняется перед каждым тестом в жтой секции
         *
         * Создаем тестового пользователя в нашей базе данных.
         * и даем ему email из константы TEST_EMAIL
         */
        before((done) => {
            new User({ email: TEST_EMAIL, password: "XWuiYdlj" })
                .save()
                .then(() => done());
        });

        /**
         * Это выполняется перед каждым тестом в жтой секции
         *
         * Удаляем тестового пользователя, чтобы потом можно
         * было еще раз запустить тест :)
         */
        after((done) => {
            User.find({ email: TEST_EMAIL })
                .remove()
                .exec()
                .then(() => done());
        });

        /**
         * Пытаемся зарегистрироваться с e-mail'ом который уже
         * занят.
         */
        it('#is user exists error', (done) => {
            agent
                .post(REGISTRATION_URL)
                .send({
                    email: TEST_EMAIL,
                    password: 'XWuiYdlj'
                })
                .end((err, res) => {
                    const response = JSON.parse(res.text);

                    assert.equal(400, res.statusCode);

                    assert.isDefined(response.data.description);
                    assert.equal("A user with this email address already exist", response.data.description);

                    done();
                });
        });
    });

    /**
     * Проверяем успешную регистрацию пользователя в системе
     */
    describe('-> successRegistration', () => {
        let email = "test@elcamino.edu";
        let password = "XWuiYdlj";

        /**
         * Посылаем запрос на регистрацию, и проверям,
         * что статус success и statusCode = 200
         */
        it('user success registration', (done) => {
            agent
                .post(REGISTRATION_URL)
                .send({ email, password })
                .end((err, res) => {
                    assert.equal(200, res.statusCode);

                    const response = JSON.parse(res.text);
                    assert.isDefined("data not defined in response", response.data);
                    assert.isDefined("status undefined in response", response.data.status);

                    assert.equal("success", response.data.status);

                    done();
                });
        });

        /**
         * После прохода теста, удаляем нашего тестового
         * пользователя, чтобы базу не засерать каждым
         * прогоном тестов.
         */
        after(() => {
            User.find({ email }).remove().exec();
        })
    });
});