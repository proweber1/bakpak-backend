'use strict';

const chai = require('chai')
    , assert = chai.assert
    , app = require('../../app')
    , chaiHttp = require('chai-http')
    , User = require('../../models/User')
    , Crypto = require('../../utils/Crypto');

chai.use(chaiHttp);

const agent = chai.request.agent(app);
const AUTH_URL = "/api/v1/auth";

/**
 * Тестирование контроллера авторизации
 */
describe("AuthController", () => {
    /**
     * Проверяем ошибки валидации, такие как, пользователя не существует
     * или ошибку типа "Invalid credentials"
     */
    describe("-> validation errors", () => {
        it("#userNotPresent", (done) => {
            agent.post(AUTH_URL).send({
                email: 'dsfdsfkdsf@elcamino.edu',
                password: 'qwerty'
            }).end((err, res) => {
                let response_json = JSON.parse(res.text);

                assert.equal(404, res.statusCode);
                assert.equal("User not found in system", response_json.data.description);

                done();
            });
        });
    });

    /**
     * Проверяем корректность авторизации
     */
    describe("-> credentials", () => {
        const TEST_EMAIL = "testing@elcamino.edu";
        const TEST_PASS = "XWuiYdlj";

        /**
         * Перед выполнением всех тестов создаем тестового
         * пользователя.
         */
        beforeEach((done) => {
            new User({
                email: TEST_EMAIL,
                password: Crypto.sha256hash(TEST_PASS)
            }).save().then(() => done());
        });

        /**
         * После прохода всех тестов удаляем тестового пользователя,
         * чтобы не засерать базу данных.
         */
        afterEach((done) => {
            User.find({ email: TEST_EMAIL }).remove().exec().then(() => done());
        });

        /**
         * Пытаемся авторизироваться с неверными данными, ожидается ошибка
         * с сообщением о неверных данных и статус кодом 401.
         */
        it("#invalidCredentials", (done) => {
            agent.post(AUTH_URL).send({
                email: TEST_EMAIL,
                password: 'bakpak'
            }).end((err, res) => {
                let response_json = JSON.parse(res.text);

                assert.equal(401, res.statusCode);
                assert.equal("User invalid credentials", response_json.data.description);

                done();
            });
        });

        /**
         * Если авторизация прошла успешно, то проверяем схему ответа,
         * проверям то, что access_token есть и есть модель person без
         * пароля.
         */
        it("#authComplete", (done) => {
            agent.post(AUTH_URL).send({
                email: TEST_EMAIL,
                password: TEST_PASS
            }).end((err, res) => {
                let data = (JSON.parse(res.text)).data;

                assert.equal(200, res.statusCode);
                assert.isDefined(data.access_token);
                assert.isDefined(data.person);

                assert.isDefined(data.person._id);
                assert.isDefined(data.person.email);
                // Нивкоем случае нельзя отдавать пароль в ответе
                assert.isUndefined(data.person.password);

                assert.equal(TEST_EMAIL, data.person.email);

                done();
            });
        });
    });
});