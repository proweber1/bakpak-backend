'use strict';

const mongoose = require('../mongoose');

const UserSchema = mongoose.Schema({
    email: {
        type: String,
        match: /^\w+([\.-]?\w+)*@elcamino.edu+$/,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    username: String,
    email_confirmed: Boolean,
    email_confirm_token: String
}, { versionKey: false });

module.exports = mongoose.model('User', UserSchema);