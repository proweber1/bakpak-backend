'use strict';

const mongoose = require('../mongoose');

const BookSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: String,
    category: String,
    condition: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    images: Array,
    isbn: String,
    owner: {
        type: String,
        required: true
    }
}, { versionKey: false });

module.exports = mongoose.model('Book', BookSchema);