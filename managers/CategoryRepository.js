'use strict';

const Optional = require('../modules/api/java/Optional');
const Category = require('../models/Categories');

class CategoryRepository {
    static async getCategoryById(id) {
        let category;

        try {
            category = await Category.findById(id);
        } catch (error) {
            console.log(error.message);
        }

        return Optional.valueOf(category);
    }
}

module.exports = CategoryRepository;