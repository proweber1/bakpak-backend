'use strict';

const User = require('../models/User')
    , Crypto = require('../utils/Crypto')
    , Optional = require('../modules/api/java/Optional')
    , randomString = require('randomstring');

/**
 * Этот класс инкапсулирует в себе работу с моделью User.
 * У него неудачное название, оно будет переименовано
 * в следующих релизах.
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 November
 *
 * @deprecated
 */
class UserManager {

    /**
     * Создает нового пользователя по его email'у и
     * паролю, пароль хэшируется в sha256 с солью.
     *
     * @param email
     * @param password
     * @returns {*}
     */
    static createUser(email, password) {
        return new User({
            email: email,
            password: Crypto.sha256hash(password),
            email_confirmed: false,
            email_confirm_token: randomString.generate(),
            username: email.split('@')[0].split('_')[0]
        }).save();
    }

    /**
     * Ищет пользователя по e-mail
     *
     * @param email
     * @returns {Optional}
     */
    static async findUser(email) {
        let user = await User.findOne({ email });

        return Optional.valueOf(user);
    }

    /**
     * Ищет определенного пользователя по токену из
     * параметра урла.
     *
     * @param token
     * @returns {Optional}
     */
    static async findUserByToken(token) {
        let user = await User.findOne({ email_confirm_token: token });

        return Optional.valueOf(user);
    }

    /**
     * Ставит пользователю флажок, что почта успешно подтверждена.
     * Удаляем у него токен также.
     *
     * @param user
     */
    static async confirmEmail(user) {
        user.email_confirm_token = undefined;
        user.email_confirmed = true;

        return user.save();
    }

    /**
     * Ищет пользователя по ID.
     *
     * @param id
     * @returns {*|Query}
     */
    static async getUserById(id) {
        let person = await User.findById(id);

        return Optional.valueOf(person);
    }
}

module.exports = UserManager;