'use strict';

const Book = require('../models/Book');
const Optional = require('../modules/api/java/Optional');
const CategoryRepository = require('./CategoryRepository');
const UserRepository = require('./UserManager');
const PaginationConstants = require('./constants/Pagination');

/**
 * Репозиторий для работы с книгами
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 November
 */
class BookRepository {

    /**
     * Создает новую книгу, сохраняет ее в базу данных.
     * Указывает ей продавца в поле owner. Это текстовый
     * ID продавца.
     *
     * @param bookData
     * @param ownerId
     * @returns {*}
     */
    static create(bookData, ownerId) {
        bookData.owner = ownerId;

        return new Book(bookData).save();
    }

    /**
     * Возвращает опционал книги по ее ID. Сначала ищет книгу,
     * а потом возвращает объект Optional для этой книги
     *
     * @param id
     * @returns {Optional}
     */
    static async findById(id) {
        let book = await Book.findById(id);

        return Optional.valueOf(book);
    }

    /**
     * Возвращает список книг определенного пользователя по
     * owner id.
     *
     * @param ownerId
     * @param limit
     * @param offset
     * @returns {*}
     */
    static getBooksByOwner(ownerId, limit = PaginationConstants.LIMIT, offset = PaginationConstants.OFFSET) {
        return BookRepository.getBooksByCondition({ owner: ownerId }, limit, offset);
    }

    /**
     * Возвращает все книги и ищет их по кодишену который передали
     * в метод
     *
     * @param condition
     * @param limit
     * @param offset
     * @returns {*}
     */
    static getBooksByCondition(
        condition = {}, limit = PaginationConstants.LIMIT,
        offset = PaginationConstants.OFFSET) {

        return Book.find(condition)
            .skip(offset)
            .limit(limit);
    }

    /**
     * Метод для поиска книг в хранилище по определенному условию.
     *
     * @param pagination
     * @param filter
     */
    static search(pagination, filter) {
        return BookRepository.getBooksByCondition(

            BookRepository._getConditionByFilter(filter),
            pagination.limit,
            pagination.offset
        );
    }

    /**
     * Возвращает книгу с подгруженными к ней связями
     *
     * @param id
     * @returns {*}
     */
    static async getBookByIdWithRelations(id) {
        let book = await BookRepository.findById(id);

        if (!book.isPresent())
            return Optional.nullable();

        book = await BookRepository.getBookRelations(book.get().toJSON());

        return Optional.valueOf(book);
    }

    /**
     * Возвращает связи книги.
     *
     * @param bookModel
     */
    static async getBookRelations(bookModel) {
        const seller = await UserRepository.getUserById(bookModel.owner);
        const category = await CategoryRepository.getCategoryById(bookModel.category);

        bookModel.seller = seller.get();
        bookModel.category = category.get();

        return bookModel;
    }

    /**
     * Собирает объект с условием для нескольких методов
     *
     * @param filter
     * @returns {{}}
     * @private
     */
    static _getConditionByFilter(filter) {
        let condition = {};

        if (filter && typeof(filter) === 'string') {
            condition.$or = [{ title: new RegExp('^.*'+filter+'.*$', 'i') }];
        }

        return condition;
    }

    /**
     * Возвращает количество книг по условию.
     *
     * @param filter
     * @returns {*}
     */
    static getBooksCountByFilter(filter) {
        return Book.count(BookRepository._getConditionByFilter(filter));
    }
}

module.exports = BookRepository;