'use strict';

const config = require('./config/db.json');

module.exports = process.env.NODE_ENV !== "test" ? config.db : config.db_test;