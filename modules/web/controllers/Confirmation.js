'use strict';

const UserManager = require('../../../managers/UserManager');

/**
 * Контроллер который позволяет подтвердить e-mail
 * пользователя.
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 November
 */
class Confirmation {

    /**
     * Подтверждаем email пользователя. Токен подтверждения
     * находится в параметрах в path.
     *
     * TODO: Отрефакторить рендеринг страницы
     *
     * @param req
     * @param res
     * @param next
     */
    static async confirm(req, res, next) {
        const confirm_token = req.param('token');

        (await UserManager.findUserByToken(confirm_token))
            .ifPresent((user) => {
                UserManager.confirmEmail(user);

                return res.render('web_confirm.hbs', {
                    status: 'like',
                    title: 'Great Job!',
                    message: 'Your e-mail successfully confirmed.'
                });
            })
            .orElseLambda(() => {
                return res.render('web_confirm.hbs', {
                    status: 'bad',
                    title: 'User not found!',
                    message: 'User with token: token not found on the system, if you came here by ' +
                    'accident, or you are a hacker, close the page on good :)'
                });
            });
    }
}

module.exports = Confirmation;