'use strict';

class UrlBuilder {
    static getConfirmationUrl(token) {
        return token ? '/web/confirm/' + token : false;
    }
}

module.exports = UrlBuilder;