'use strict';

const express = require('express')
    , router = express.Router();

/*
 * #####################################################################
 * #                     All controllers require                       #
 * #####################################################################
 */
const confirmation_controller = require('./controllers/Confirmation');

router.get('/confirm/:token', confirmation_controller.confirm);

module.exports = router;