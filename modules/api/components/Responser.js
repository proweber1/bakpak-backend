'use strict';

/**
 * Класс который работает с форматами ответов для клиента, возможно
 * более лучшей идеей было бы сделать этот класс базовым контроллером
 * для всех остальных контроллеров. Но есть предположение что он
 * будет использоваться и в других местах, помимо контроллеров.
 *
 * Так же композиция лучше чем наследование :)
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 November
 */
class Responser {

    /**
     * Обромляет переданные данные в метод в специальную форму
     *
     * @param res
     * @param data
     * @param statusCode
     * @returns {*|Request}
     */
    static response(res, data, statusCode) {
        if (statusCode)
            res.status(statusCode);

        return res.send(data);
    }

    /**
     * Этот метод инкапсулирует в себе модель ответа CompleteOperation
     * из документации. Сделано так, чтобы везде где это нужно НЕ
     * писать следующий код
     *
     * Responser.response({
     *    status: 'success'
     * });
     *
     * А можно было бы просто
     *
     * Responser.successResponse()
     *
     * @param res
     * @returns {*|Request}
     */
    static successResponse(res) {
        return Responser.response(res, {
            status: 'success'
        });
    }
}

module.exports = Responser;