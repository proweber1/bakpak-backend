'use strict';

const express = require('express')
    , router = express.Router()
    , validator = require('../../override/node-validator/validator');

const ApiController = require('./controllers/ApiController')
    , RegistrationController = require('./controllers/RegistrationController')
    , AuthController = require('./controllers/AuthController')
    , UploadController = require('./controllers/ImagesController')
    , BookController = require('./controllers/BooksController')
    , CategoriesController = require('./controllers/CategoriesController')
    , StripeController = require('./controllers/StripeControler')
    , UserController = require('./controllers/UserController');

const regAuthValidator = require('./request_validators/RegAuthRequest');
const bookRequestValidator = require('./request_validators/BookRequest');
const stripeRequestValidator = require('./request_validators/StripeRequest');

const PaginationConst = require('../../managers/constants/Pagination');

const corsConfigration = require('./config/cors.json');

/*
Устанавливаем всем ответам заголовки Access-* и Content-type
равный application/json; charset=UTF-8
 */
router.use((req, res, next) => {
    res.type('json');

    res.set({
        'Access-Control-Allow-Methods': corsConfigration.accessControlAllowMethods,
        'Access-Control-Allow-Origin': corsConfigration.accessControlAllowOrigin,
        'Access-Control-Allow-Headers': corsConfigration.accessControlAllowHeaders
    });

    next();
});

// Options for all actions
router.options('/v1/*', (req, res, next) => res.end());

// Api routes
router.get('/v1', ApiController.indexAction);

// post actions
router.post('/v1/registration', [
    validator.express(regAuthValidator()),
    RegistrationController.registrationAction
]);

router.post('/v1/auth', [
    validator.express(regAuthValidator()),
    AuthController.login
]);

/* ######################################################################
 * #                          Secured area                              #
 * ######################################################################
 */
router.use(require('./filters/Auth'));
router.use((req, res, next) => {
    req.pagination = {
        limit:  parseInt(req.query.limit  || PaginationConst.LIMIT),
        offset: parseInt(req.query.offset || PaginationConst.OFFSET)
    };

    next();
});

router.post('/v1/images', [UploadController.beforeUpload, UploadController.upload]);

router.post('/v1/books', [
    validator.express(bookRequestValidator()),
    BookController.beforeCreateAndUpdate,
    BookController.create
]);

router.get('/v1/categories', CategoriesController.list);

router.get('/v1/books', BookController.userBooks);

router.get('/v1/books/search', BookController.search);

let bookCredentialsFilter = require('./filters/CheckBookCredentials');
router.get('/v1/books/:id', BookController.view);

router.delete('/v1/books/:id', [
    bookCredentialsFilter,
    BookController.deleteBook
]);

router.patch('/v1/books/:id', [
    bookCredentialsFilter,
    BookController.beforeCreateAndUpdate,
    BookController.updateBook
]);

router.post('/v1/stripe', [
    validator.express(stripeRequestValidator()),
    StripeController.transaction
]);

router.get('/v1/user', UserController.userByToken);

module.exports = router;