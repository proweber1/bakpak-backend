'use strict';

const BookRepository = require('../../../managers/BookRepository');
const HttpException = require('../exceptions/HttpException');

module.exports = async (req, res, next) => {
    (await BookRepository.findById(req.param('id')))
        .ifPresent((book) => {
            if (book.owner !== req.user.id)
                return next(new HttpException('You do not have access to this book', 403));

            req.book = book;
            return next();
        })
        .orElseLambda(() => next(new HttpException('Book not found!', 404)));
};