'use strict';

const HttpException  = require('../exceptions/HttpException')
    , JwtService     = require('../services/JwtService')
    , UserRepository = require('../../../managers/UserManager');

const microserviceToken = "JavaStripeV2TokenXWuiYdlj$20122013";

function isMicroserviceToken(token) {
    return microserviceToken === token;
}

function getMicroserviceUser() {
    return {
        id: "aaaaaaaaaaaaaaa",
        email: "ZxCwsFerSd7@stripe_microservice.com",
        iat: 1204355
    };
}

module.exports = async (req, res, next) => {
    const auth_token = req.get('Authorization');
    if (undefined === auth_token) {
        return next(new HttpException('Please, authorize!', 401));
    }

    if (isMicroserviceToken(auth_token)) {
        req.user = getMicroserviceUser();
        return next();
    }

    let user = null;

    try {
        user = JwtService.decodeUser(auth_token);
    } catch (e) {
        return next(new HttpException(e.message, 500));
    }

    const userOptional = await UserRepository.getUserById(user.id);

    if (!userOptional.isPresent()) {
        return next(new HttpException('User not found', 404));
    }

    req.user = user;

    return next();
};