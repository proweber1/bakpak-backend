'use strict';

const jwt = require('jsonwebtoken')
    , main_conf = require('../config/main.json');

/**
 * Этот сервис работает с Json Web Token API, он инкапсулирует
 * логику encode'а пользователя и decode'а его же из JWT.
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 November
 */
class JwtService {

    /**
     * Создает JWT исходя из модели пользователя
     * и возвращает получившийся токен.
     *
     * @param user_model
     * @returns {*}
     */
    static encodeUser(user_model) {
        return jwt.sign({
            id: user_model.id,
            email: user_model.email
        }, main_conf.jwt_signature);
    }

    /**
     * Дешефриует JWT токен и извлекает из него
     * пользователя.
     *
     * @param token
     * @returns {*}
     */
    static decodeUser(token) {
        return jwt.verify(token, main_conf.jwt_signature);
    }
}

module.exports = JwtService;