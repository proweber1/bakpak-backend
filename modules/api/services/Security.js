'use strict';

const Crypto = require('../../../utils/Crypto');

/**
 * Этот класс предназначен для работы с безопасностью.
 * Такой как проверки паролей, хешей и так далее
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 Vjacheslav
 */
class Security {

    /**
     * Валидирует пароль.
     *
     * В этот метод передается на захэшированный пароль и
     * хэш с которым надо сравнить пароль. Пароль хэшируется
     * по принятому стандарту в проекте (sha256) и дальше
     * эти два хэша сравниваются между собой.
     *
     * @param password
     * @param hash
     * @returns {boolean}
     */
    static validatePassword(password, hash) {
        const password_hash = Crypto.sha256hash(password);

        return hash === password_hash;
    }
}

module.exports = Security;