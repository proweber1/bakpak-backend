'use strict';

const parameters = require('../../../config/parameters.json');
const path = require('path');
const appRootPath = require('app-root-dir');

class NamingStrategy {
    static generateFileName(originalFileName) {
        return originalFileName.split(' ').join('_');
    }
}

/**
 * Это сервис который позволяет загружать файлы на сервер.
 * Он не совсем универсален. Но можно уницифировать. Без
 * проблем.
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 November
 */
class UploadService {

    /**
     * Принимает список файлов для загрузки
     *
     * @param files
     */
    constructor(files) {
        this.files = files;
    }

    /**
     * Загружает файлы на сервер. Возвращает имя
     * загруженного файла.
     *
     * @returns {Promise<*>}
     */
    upload() {
        let filePromises = [
            'photo1',
            'photo2',
            'photo3'
        ].map((index) => this._uploadSingleImage(index));

        return Promise.all(filePromises);
    }

    /**
     * Загружает одно конкретное изображение на сервер.
     *
     * @param index
     * @returns {Promise}
     * @private
     */
    _uploadSingleImage(index) {
        return new Promise((resolve, reject) => {
            const file = this.files[index];
            const fileName = (new Date().getTime()) + '_' + NamingStrategy.generateFileName(file.name);

            file.mv(path.join(
                appRootPath.get(),
                parameters.upload_path,
                fileName
            ), (err) => err ? reject(err) : resolve(fileName));
        });
    }
}

module.exports = UploadService;