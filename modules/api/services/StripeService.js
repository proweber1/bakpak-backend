'use strict';

const request = require('request');
const config = require('../../../config/parameters.json');

/**
 * Сервис который работает с микросервисом
 * страйпа.
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016
 */
class StripeService {

    /**
     * Посылает POST запрос к микросервису который работает со
     * страйпом и возвращает от него ответ в виде Promise.
     *
     * @param token Токен для создания Charge
     * @param amount Сумма на которую надо сделать Charge
     *
     * @returns {Promise}
     */
    static pay(token, amount) {
        return new Promise((resolve, reject) => {
            request({
                url: config.microServices.stripe + 'pay',
                method: 'POST',
                form: { token, amount }
            }, (err, res, res_body) => {
                if (err)
                    return reject(err);
                resolve(res_body);
            })
        });
    }
}

module.exports = StripeService;