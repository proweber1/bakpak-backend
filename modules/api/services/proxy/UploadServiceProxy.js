'use strict';

const UploadService = require('../UploadService');
const parameters = require('../../../../config/parameters.json');

/**
 * Это прокси который проверяет типы загружаемых файловю.
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 November
 */
class UploadServiceProxy extends UploadService {

    /**
     * В этом методе происходит валидация загружаемых файлов
     *
     * @returns {Array}
     */
    upload() {
        // if (!this._isContainsMimeTypeInList()) {
        //     throw new Error('You tried to upload a file');
        // }

        return super.upload();
    }

    /**
     * Проверяет, является ли файл допустимым к загрузке.
     * Работает на основе обычных mime типов.
     *
     * @private
     */
    _isContainsMimeTypeInList() {
        return (-1 !== parameters.upload_file_types.indexOf(this.files.images.mimetype));
    }
}

module.exports = UploadServiceProxy;
