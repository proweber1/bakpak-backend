'use strict';

const parameters = require('../../../config/parameters.json')
    , UrlBuilder = require('../../web/utils/UrlBuilder');

/**
 * Этот сервис не тестируется. Так как он использует стороннюю
 * либу и проверить то, доставилось ли сообщение, невозможно.
 * Хотя можно при тестировании писать письма в файл.
 *
 * TODO: Написать тест
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 October
 */
class Mails {

    /**
     * Отправляет ссылку для подтверждения e-mail'а.
     * Пользователю.
     *
     * @param user
     */
    static sendRegistrationConfirmLink(user) {
        if (user.email_confirmed)
            return ;

        mailer.send('registration.hbs', {
            to: user.email,
            subject: 'Email confirm',
            confirm_link: parameters.host + UrlBuilder.getConfirmationUrl(user.email_confirm_token)
        }, (err) => console.log(err));
    }
}

module.exports = Mails;