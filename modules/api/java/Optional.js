'use strict';

/**
 * Аналог java.util.Optional
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 November
 */
class Optional {

    /**
     * Конструктор класса Optional
     *
     * @param value Значение хранимое в классе
     */
    constructor(value) {
        this.value = value;
    }

    /**
     * Статический метод для создания нового
     * объекта Optional
     *
     * @param value значение которое будет храниться
     * @returns {Optional}
     */
    static valueOf(value) {
        return new Optional(value);
    }

    /**
     * Возвращает опционал с нулевым значением,
     * чтобы не делать некрасивые конструкции типа
     *
     * <code><pre>
     * Optional.valueOf(null)
     * </pre></code>
     *
     * @returns {Optional}
     */
    static nullable() {
        return new Optional(null);
    }

    /**
     * Этот метод возвращает значение defaultVal если значения
     * в классе optional равно null или undefined или 0 и так далее
     *
     * @param defaultVal значение которое будет использовано вместо null
     * @returns {*}
     */
    orElse(defaultVal) {
        return !this.isPresent() ? defaultVal : this.value;
    }

    /**
     * Это аналог orElse только вместо значения этот метод принимает
     * лямбду которая должна вернуть значение.
     *
     * @param func
     * @returns {*}
     */
    orElseGet(func) {
        return this.orElse(func());
    }

    /**
     * Возвращает значение которое хранится внутри Optional объекта
     *
     * @returns {*}
     */
    get() {
        return this.value;
    }

    /**
     * Если значения в Optional нет, то выход из функции, а если
     * есть, то вызываем лямбду которую передали в метод и передаем
     * в нее значение содержащееся в классе.
     *
     * @param func
     */
    ifPresent(func) {
        if (!this.isPresent())
            return this;

        func(this.value);

        return this;
    }

    /**
     * Если значение не установлено, вызовет нашу лямбду
     *
     * @param lambda
     */
    orElseLambda(lambda) {
        if (!this.isPresent())
            return lambda();

        return false;
    }

    /**
     * Возвращает булево true если значение в классе установлено и
     * false если нет.
     *
     * @returns {boolean}
     */
    isPresent() {
        return (null !== this.value && undefined !== this.value);
    }
}

module.exports = Optional;