'use strict';

const Response = require('../components/Responser');

class UserController {

    /**
     * Возвращает пользователя по токену который есть в заголовке,
     * это так скажем хак, ибо этот метод вызвается из другого микросервиса
     * и просто в заголовке передает <code>Authorization</code> заголовок
     * с помощью которого происходит авторизация, авторизированный юзер
     * появляется в req.user, соответственно надо просто вернуть его.
     *
     * @param req Объект Request
     * @param res Объект Response
     * @param next Функция которой можно вызвать следующий обработчик
     * @returns {*|Request}
     */
    static userByToken(req, res, next) {
        return Response.response(res, req.user);
    }
}

module.exports = UserController;