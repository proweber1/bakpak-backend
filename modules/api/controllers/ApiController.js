'use strict';

class ApiController {
    static indexAction(req, res, next) {
        res.json({ info: "ok" });
    }
}

module.exports = ApiController;