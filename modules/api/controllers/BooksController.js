'use strict';

const HttpException = require('../exceptions/HttpException');
const CategoryRepository = require('../../../managers/CategoryRepository');
const BookRepository = require('../../../managers/BookRepository');
const Responser = require('../components/Responser');
const BookDecorator = require('../decorators/schema_decorators/Book');

class BooksController {

    /**
     * Возвращает список всех книг текущего авторизированного
     * пользователя.
     *
     * TODO: Refactor this
     *
     * @param req
     * @param res
     * @param next
     * @returns {*|Request}
     */
    static async userBooks(req, res, next) {
        let limit = parseInt(req.query.limit),
            offset = parseInt(req.query.offset);

        let books = await BookRepository.getBooksByOwner(req.user.id, limit, offset);

        return Responser.response(res, await BooksController._bookProcessor(books, req.user));
    }

    /**
     * Универсальный метод который используется в нескольких
     * экшенах в этом контроллере
     *
     * @param books
     * @param user
     * @returns {*}
     * @private
     */
    static async _bookProcessor(books, user) {
        let bookRelative = await Promise.all(
            books.map(book => BookRepository.getBookRelations(book.toJSON()))
        );

        return await Promise.all(
            bookRelative.map(book => new BookDecorator(book, user).decorate())
        );
    }

    /**
     * Перед сохранением книги, проверяем существование
     * категории.
     *
     * @param req
     * @param res
     * @param next
     */
    static async beforeCreateAndUpdate(req, res, next) {
        if (!req.body.category)
            return next();

        (await CategoryRepository.getCategoryById(req.body.category))
            .ifPresent(() => next())
            .orElseLambda(() => next(new HttpException("Category not found", 400)));
    }

    /**
     * Создаем новую книгу.
     *
     * @param req
     * @param res
     * @param next
     */
    static async create(req, res, next) {
        try {
            let book = await BookRepository.create(req.body, req.user.id);
            book = await BookRepository.getBookRelations(book.toJSON());

            // Decorate Book model, remove category_id
            // to category model. It not best practise.
            // please, use Schema.post('init', ...)
            const bookDecorator = new BookDecorator(book, req.user);

            return Responser.response(res, await bookDecorator.decorate());
        } catch (error) {
            return next(new HttpException(error.message));
        }
    }

    /**
     * Отображение книги
     *
     * @param req
     * @param res
     * @param next
     * @returns {*|Request}
     */
    static async view(req, res, next) {
        let book;

        try {
            book = await BookRepository.getBookByIdWithRelations(req.params.id);
        } catch (e) {
            return next(e);
        }

        book.ifPresent(async (book) => {
            let bookDecorator = new BookDecorator(book, req.user);

            return Responser.response(res, bookDecorator.decorate())
        });

        book.orElseLambda(() => next(new HttpException('Book not found', 404)));
    }

    /**
     * Удаляет книгу из базы данных
     *
     * @param req
     * @param res
     * @param next
     */
    static deleteBook(req, res, next) {
        req.book.remove();

        return Responser.successResponse(res);
    }

    /**
     * Обновляет определенную книгу пользователя. Текущая книга
     * которая загрузилась по ID находится в req.book. Ее обновляем
     * параметрами из req.body.
     *
     * @param req
     * @param res
     * @param next
     * @returns {*|Request}
     */
    static async updateBook(req, res, next) {
        try {
            await req.book.update(req.body);

            (await BookRepository.getBookByIdWithRelations(req.params.id))
                .ifPresent(async book => {
                    let decoratedBook = await new BookDecorator(book).decorate();

                    return Responser.response(res, decoratedBook);
                })
                .orElseLambda(_ => next(new HttpException('Book not found :(', 404)));
        } catch(e) {
            return next(new HttpException(e.message));
        }
    }

    /**
     * Этот экшен используется для поиска книг в системе, отличие
     * этого метода от <code>GET /books</code> в том, что этот метод
     * возвращает книги во всей системе которые можно фильтровать.
     *
     * @param req
     * @param res
     * @param next
     */
    static async search(req, res, next) {
        let filter = req.query.filter,
            books  = await BookRepository.search(req.pagination, filter);

        return Responser.response(res, {
            books: await BooksController._bookProcessor(books, req.user),
            meta: {
                count: await BookRepository.getBooksCountByFilter(filter),
                limit: req.pagination.limit,
                offset: req.pagination.offset
            }
        });
    }
}

module.exports = BooksController;