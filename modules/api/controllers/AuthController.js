'use strict';

const UserManager = require('../../../managers/UserManager')
    , HttpException = require('../exceptions/HttpException')
    , Security = require('../services/Security')
    , JwtService = require('../services/JwtService')
    , Responser = require('../components/Responser')
    , PersonSchema = require('../decorators/schema_decorators/User');

/**
 * Это класс контроллера который несет в себе экшены
 * авторизации, мапится он на следующий endpoint.
 * <CODE>/auth</CODE> Посмотреть список всех
 * endpoint'ов можно в файле <b>routes.js</b>
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 November
 */
class AuthController {

    /**
     * Авторизация пользователя в системе, используется JWT.
     * Здесь мы делаем запрос в базу данных, нам возвращается
     * Optional. Если значение в нем установленно, то мы проверяем
     * значение пароля, если все ок, то удаляем из схемы
     * пароль и возвращаем схему клиенту
     *
     * Если клиент не установлен, то мы скидываем ошибку 404 с
     * информацией о том, что клиент не найден
     *
     * @param req
     * @param res
     * @param next
     */
    static async login(req, res, next) {
        (await UserManager.findUser(req.body.email))
            .ifPresent((user) => {
                const request_pass = req.body.password;

                if (!Security.validatePassword(request_pass, user.password)) {
                    return next(new HttpException("User invalid credentials", 401));
                }

                /*
                Возможно это странная конструкция, но удалять пароль на уровне
                выборки не совсем правильно, так как он может пригодиться в
                других местах, а в некоторых нет, по-этому здесь есть эта конструкция
                 */
                const person = PersonSchema.getUserSchema(user);
                person.access_token = JwtService.encodeUser(user);

                return Responser.response(res, person);
            })
            .orElseLambda(() => {
                return next(new HttpException("User not found in system", 404));
            });
    }

    /**
     * Простой метод который возвращает текущего авторизированного
     * пользователя по запросу.
     *
     * @param req
     * @param res
     * @param next
     * @returns {*|Request}
     */
    static userByToken(req, res, next) {
        return Responser.response(res, req.user);
    }
}

module.exports = AuthController;