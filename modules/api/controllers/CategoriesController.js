'use strict';

const Responser = require('../components/Responser');
const Categories = require('../../../models/Categories');

class CategoriesController {
    static async list(req, res, next) {
        const categories = await Categories.find();

        return Responser.response(res, categories);
    }
}

module.exports = CategoriesController;