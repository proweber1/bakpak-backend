'use strict';

const Responser = require('../components/Responser');
const HttpException = require('../exceptions/HttpException');
const UploadProxy = require('../services/proxy/UploadServiceProxy');
const UploadServiceDecorator = require('../decorators/UploadServiceDecorator');

/**
 * Контроллер который работает с загрузкой изображений
 * на сервер.
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 November
 */
class ImagesController {

    /**
     * Вызывается в роутере перед экшеном загрузки
     * файлов. Данный метод проверяет факт нахождения
     * файлов в запросе. Если их нет, передает ошибку
     * дальше по цепочке до error handler'а.
     *
     * @param req
     * @param res
     * @param next
     * @returns {*}
     */
    static beforeUpload(req, res, next) {
        if (!req.files) {
            return next(new HttpException('Files not found in request', 400));
        }

        return next();
    }

    /**
     * Загружает файл на сервер. Вызывает в себе UploadService
     * декорирует его специальным декоратом который все загруженные
     * файл добавляет к ним ссылку на сервере.
     *
     * @param req
     * @param res
     * @param next
     * @returns {*}
     */
    static async upload(req, res, next) {
        try {
            let imageLinks = (await new UploadServiceDecorator(new UploadProxy(req.files)).upload());

            return Responser.response(res, imageLinks);
        } catch (error) {
            return next(new HttpException(error.message, 400));
        }
    }
}

module.exports = ImagesController;