'use strict';

const request = require('request');
const BookRepository = require('../../../managers/BookRepository');
const HttpException = require('../exceptions/HttpException');
const Responser = require('../components/Responser');
const StripeService = require('../services/StripeService');

class StripeController {

    /**
     * Создает новую транзакцию в страйпе, делает запрос
     * на микросервис страйпа, а он в свою очередь делает
     * запрос в страйп и логирует некоторую информацию о
     * своих запросах к нему.
     *
     * @param req
     * @param res
     * @param next
     * @returns {*}
     */
    static async transaction(req, res, next) {
        try {
            (await BookRepository.findById(req.body.book_id)).ifPresent(async (book) => {
                const token = req.body.stripe_token
                    , amount = book.price;

                const serviceResult = await StripeService.pay(token, amount)
                    , json = JSON.parse(serviceResult);

                return json.statusCode
                    ? Responser.response(res, json, json.statusCode)
                    : Responser.response(res, json)
            }).orElseLambda(_ => {
                const exception = new HttpException('Book not found in system', 404);
                return next(exception);
            })
        }
        catch (err) {
            const httpException = new HttpException(err.message);
            return next(httpException);
        }
    }
}

module.exports = StripeController;