'use strict';

const UserManager = require('../../../managers/UserManager')
    , Optional = require('../java/Optional')
    , HttpException = require('../exceptions/HttpException')
    , Responser = require('../components/Responser')
    , Mailer = require('../services/Mails');

class RegistrationController {

    /**
     * Регистрация нового пользователя в системе.
     * Используется наш новомодный класс Optional.
     *
     * @param req
     * @param res
     * @param next
     */
    static async registrationAction(req, res, next) {
        (await UserManager.findUser(req.body.email))
            .ifPresent(() => {
                return next(new HttpException('A user with this email address already exist', 400));
            })
            .orElseLambda(async () => {
                const newUser = await UserManager.createUser(req.body.email, req.body.password);
                Mailer.sendRegistrationConfirmLink(newUser);

                return Responser.successResponse(res);
            });
    }
}

module.exports = RegistrationController;