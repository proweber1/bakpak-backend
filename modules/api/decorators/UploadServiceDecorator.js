'use strict';

const parameters = require('../../../config/parameters.json');
const path = require('path');

/**
 * Это декоратор который декорирует сервис для
 * загрузки изображений на сервер
 *
 * @author Vjacheslav Gusser <w.gusser@mail.ru>
 * @copyright (c) 2016 November
 */
class UploadServiceDecorator {
    constructor(uploadService) {
        this.uploadService = uploadService;
    }

    /**
     * Этот метод вызывает метод upload докерируемого
     * объекта, после чего ко всем файлам добавляет
     * ссылку текущего хоста.
     *
     * @returns {*}
     */
    async upload() {
        let fileNames = await this.uploadService.upload();

        return fileNames.map((fileName) => parameters.host + '/' + fileName);
    }
}

module.exports = UploadServiceDecorator;