'use strict';

const UserSchema = require('./User');

class Book {
    constructor(model, currentUser) {
        this.model = model;
        this.currentUser = currentUser;
    }

    /**
     * Докерирует модель книги, загружает к ней релейшены.
     * Такие как категории, сейлер, владелец и так далее.
     *
     * @returns {*|{type, data}|Object|{method, url, data, headers}|string|{method, url, data}}
     */
    decorate() {
        let schema = this.model;

        if (this.currentUser) {
            schema.owner = (schema.owner == this.currentUser.id);
        }

        if (schema.seller) {
            schema.seller = UserSchema.getUserSchema(schema.seller);
        }

        return schema;
    }
}

module.exports = Book;