'use strict';

class User {

    /**
     * Возвращает схему пользователя.
     *
     * @param person
     * @returns {*}
     */
    static getUserSchema(person) {
        let user = person.toJSON();

        delete user.password;
        delete user.email_confirm_token;

        return user;
    }
}

module.exports = User;