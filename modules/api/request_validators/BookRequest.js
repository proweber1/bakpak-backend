'use strict';

const validator = require('../../../override/node-validator/validator');

/**
 * Эта функция возвращает валидатор для запросов в которых
 * присутствуют поля email и password, он универсален, здесь
 * инкапсулированы общие правила для этих двух полей, данный
 * валидатор можно расширить.
 *
 * @returns {*}
 */
module.exports = function() {
    return validator.isObject()
        .withRequired('title', validator.isString())
        .withOptional('description', validator.isString())
        .withRequired('category', validator.isString())
        .withRequired('condition', validator.isNumber())
        .withRequired('price', validator.isNumber())
        .withOptional('images', validator.isArray(validator.isString()))
        .withOptional('isbn', validator.isString());
};