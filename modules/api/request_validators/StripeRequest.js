'use strict';

const validator = require('../../../override/node-validator/validator');

module.exports = function() {
    return validator.isObject()
        .withRequired('book_id', validator.isString())
        .withRequired('stripe_token', validator.isString({
            regex: /^tok_\w+/,
            message: "Invalid stripe token"
        }));
};