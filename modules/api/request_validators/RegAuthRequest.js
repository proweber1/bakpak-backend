'use strict';

const validator = require('../../../override/node-validator/validator');

/**
 * Эта функция возвращает валидатор для запросов в которых
 * присутствуют поля email и password, он универсален, здесь
 * инкапсулированы общие правила для этих двух полей, данный
 * валидатор можно расширить.
 *
 * @returns {*}
 */
module.exports = function() {
    return validator.isObject()
        .withRequired('email', validator.isString({
            regex: /^\w+([\.-]?\w+)*@elcamino.edu+$/,
            message: 'Valid only @elcamino.edu the emails'
        }))
        .withRequired('password', validator.isString());
};