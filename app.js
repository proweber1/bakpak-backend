var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mailer = require('express-mailer');
var fileUpload = require('express-fileupload');

var app = express();

app.set('views', ['./mails', './modules/web/views']);
app.set('view engine', 'handlebars');

const public_path = path.join(__dirname, 'modules/web/public');
const files_path = path.join(__dirname, 'files');

app.use(express.static(public_path));
app.use(express.static(files_path));

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(fileUpload());

// Favicon hack
app.use('/favicon.ico', function(req, res, next) {
  res.writeHead(200, {'Content-Type': 'image/x-icon'} );
  res.end();
  console.log('favicon requested');
});

console.log("APPLICATION RUN: NODE_ENV = ", process.env.NODE_ENV || "dev");

var api_routes = require('./modules/api/routes');
var website = require('./modules/web/routes');

app.use('/api', api_routes);
app.use('/web', website);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

var Responser = require('./modules/api/components/Responser');

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);

  Responser.response(res, {
    description: err.message
  });
});

var mailer_settings = require("./config/mailer.json");

/*
Библиотека mailer делалась под express 3.x соответственно
когда пишется она в объект app.mailer она доступна только
в этом файле. Чтобы дальше по системе было легко иметь
доступ к мейлеру, мы пишем его в глобальный объект. И по
коду можно теперь использовать mailer(...)
 */
mailer.extend(app, mailer_settings);

/*
Пишем мейлер как глобальную функцию, чтоб потом по коду
можно было не делать required'ов app.js. Это на самом деле
не совсем хорошо. Но зато можно делать некоторое подобие IoC
контейнера ;-)
 */
global.mailer = app.mailer;

module.exports = app;
