'use strict';

const CategoryModel = require('../models/Categories');

exports.up = function (next) {
	console.log('    --> This is migration 2016-11-20-1345.js being applied');

	const categories = [
		{ name: 'English 82' },
		{ name: 'English 84' },
		{ name: 'English B' },
		{ name: 'English A' },
		{ name: 'English 1A' },
		{ name: 'English 1B' },
		{ name: 'English 1C' },
		{ name: 'English 1CH' },
		{ name: 'English 15B' },
		{ name: 'English as a Second Language 52B' }
	];

	categories.forEach((category) => {
		new CategoryModel(category).save();
	});

	next();
};


exports.down = function (next) {
	console.log('    --> This is migration 2016-11-20-1345.js being rollbacked');
	next();
};
